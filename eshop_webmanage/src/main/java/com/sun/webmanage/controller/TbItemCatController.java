package com.sun.webmanage.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sun.commons.pojo.EasyUITreeData;
import com.sun.commons.utils.JsonUtils;
import com.sun.webmanage.service.TbItemCatService;

@Controller
public class TbItemCatController {

	@Resource
	private TbItemCatService tbItemCatService;
	
	@ResponseBody
	@RequestMapping("/item/cat/list")
	public List<EasyUITreeData> list(@RequestParam(defaultValue="0")long id){
		List<EasyUITreeData> listdata = tbItemCatService.listEasyUITreeData(id);
		return listdata;
	}
}

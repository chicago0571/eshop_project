package com.sun.webmanage.service;

import com.sun.commons.pojo.EasyUIDataGrid;
import com.sun.exception.DaoException;
import com.sun.webmanage.model.TbItemParam;

public interface TbItemParamService {

	EasyUIDataGrid listTbItemParamPage(int pageNum,int rows);
	
	boolean deleteTbItemByPK(String ids)throws DaoException;
	
	TbItemParam loadTbItemParamByCatId(long catId);
	
	boolean saveTbItemParam(long catId,String paramData);
}
